package com.tetris.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import com.tetris.input.InputHandler;
import com.tetris.object.Board;
import com.tetris.object.NextItemPanel;
import com.tetris.object.Shape;

/**
 * Main class
 * Basic Game loop using java & swing extracted from here: http://compsci.ca/v3/viewtopic.php?t=25991
 * Tetris basic math logic mostly extracted from here: http://zetcode.com/tutorials/javagamestutorial/tetris/
 * @author Alexandro
 *
 */
public class Tetris extends JFrame {
	private static final long serialVersionUID = -5852825883028371568L;
	/**
	 * Available colors
	 */
	public static enum Colors {
		RED(Color.RED),
		GREEN(Color.GREEN),
		BLUE(Color.BLUE);
		
		private final Color color;
		private Colors(Color color) {
			this.color = color;
		}
		
		public Color getColor() {
			return color;
		}
	}
	
	/**
	 * Game speed (in milliseconds)
	 */
	public static final long DROP_LOOP_TIME = 400;
	/**
	 * Game level time droper (in milliseconds)
	 */
	public static final int DROP_TIME_DECREASER = 40;
	/**
	 * Size of the square
	 */
	public static final int SQUARE_SIZE = 20;
	/**
	 * Number of squares that fits horizontally per board
	 */
	public static final int BOARD_WIDTH = 11;
	/**
	 * Number of squares that fits vertically per board
	 */
	public static final int BOARD_HEIGHT = 20;
	/**
	 * Number of penalizations to apply
	 */
	public static final int PENALIZATION = 2;

	/**
	 * Flag for principal loop
	 */
	private static boolean isRunning = true;
	/**
	 * Number of times per seconds to do an update and refresh
	 */
	private static final int fps = 30;
	/**
	 * UI width
	 */
	private static final int windowWidth = 750;
	/**
	 * UI height
	 */
	private static final int windowHeight = 720;
	
	/**
	 * Font in game
	 */
	private Font font;
	/**
	 * Score label
	 */
	private JTextArea scoreLabel;
	/**
	 * Level label
	 */
	private JTextArea levelLabel;
	/**
	 * Allow to capture inputs with keyboard
	 */
	private InputHandler input;
	/**
	 * Boards in game
	 */
	private Board[] boards;
	/**
	 * Panel to check what's next item
	 */
	private NextItemPanel nextItemPanel;
	private NextItemPanel cpuItemPanel;
	/**
	 * Board index, index of current board where player is playing
	 */
	private int bIndex;
	/**
	 * Principal shape manager
	 */
	private Shape[] shapes = new Shape[2];
	public static final int PLAYER = 0;
	public static final int CPU = 1;
	
	/**
	 * Flag to know if game over occur
	 */
	private boolean isGameOver = false;
	/**
	 * Flag to know if game is in pause
	 */
	private boolean isPaused = true;
	/**
	 * Game score
	 */
	private long score = 0;
	/**
	 * Game level
	 */
	private int level = 0;
	
	/**
	 * Main method
	 */
	public static void main(String[] args) {
		Tetris game = new Tetris();		
		game.setDefaultCloseOperation(EXIT_ON_CLOSE);
		game.setSize(windowWidth, windowHeight);
		game.setResizable(false);
		game.setVisible(true);
		game.setTitle("Tetris");
		game.setLocationRelativeTo(null);
		
		game.run();
		System.exit(0);
	}
	
	/**
	 * Initializing UI
	 */
	public Tetris() {		
		JPanel northPanel = new JPanel();
		GridBagLayout northLayout = new GridBagLayout();
		northPanel.setLayout(northLayout);
		font = new Font(Font.MONOSPACED, Font.BOLD, 14);
		
		levelLabel = new JTextArea();
		levelLabel.setText("LEVEL: 0");
		levelLabel.setFont(font);
		levelLabel.setBackground(null);
		levelLabel.setFocusable(false);
		levelLabel.setEditable(false);
		levelLabel.setLineWrap(true);
		levelLabel.setWrapStyleWord(true);
		
		nextItemPanel = new NextItemPanel();
		cpuItemPanel = new NextItemPanel();
		
		scoreLabel = new JTextArea();
		scoreLabel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		scoreLabel.setText("SCORE: " + String.format("%015d", 0));
		scoreLabel.setFont(font);
		scoreLabel.setBackground(null);
		scoreLabel.setFocusable(false);
		scoreLabel.setEditable(false);
		scoreLabel.setLineWrap(true);
		scoreLabel.setWrapStyleWord(true);
						
		GridBagConstraints gbc = new GridBagConstraints();
		gbc = new java.awt.GridBagConstraints();
		
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.fill = java.awt.GridBagConstraints.BOTH;
        gbc.anchor = java.awt.GridBagConstraints.WEST;
        gbc.weightx = 1.0;
        gbc.insets = new java.awt.Insets(4, 4, 4, 4);
		northPanel.add(levelLabel, gbc);
		gbc = new java.awt.GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.fill = java.awt.GridBagConstraints.BOTH;
        gbc.insets = new java.awt.Insets(4, 0, 4, 0);
		northPanel.add(nextItemPanel, gbc);
		gbc = new java.awt.GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 0;
        gbc.fill = java.awt.GridBagConstraints.BOTH;
        gbc.insets = new java.awt.Insets(4, 0, 4, 0);
		northPanel.add(cpuItemPanel, gbc);
		gbc = new java.awt.GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 0;
        gbc.fill = java.awt.GridBagConstraints.BOTH;
        gbc.anchor = java.awt.GridBagConstraints.EAST;
        gbc.weightx = 1.0;
        gbc.insets = new java.awt.Insets(4, 4, 4, 4);
		northPanel.add(scoreLabel, gbc);
		
		
		// Boards setup
		JPanel centerPanel = new JPanel();
		GridBagLayout layout = new GridBagLayout();
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.BOTH;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		centerPanel.setLayout(layout);
		
		boards = new Board[3];
		for (int i = 0; i < boards.length; i++) {
			boards[i] = new Board(this);
			centerPanel.add(boards[i], constraints);
		}

		JTextArea instructions = new JTextArea();
		instructions.setBackground(null);
		instructions.setFont(font);
		instructions.setFocusable(false);
		instructions.setEditable(false);
		instructions.setLineWrap(true);
		instructions.setWrapStyleWord(true);
		instructions.setText(setInstructions());
		
		add(northPanel, BorderLayout.NORTH);
		add(centerPanel, BorderLayout.CENTER);
		add(instructions, BorderLayout.SOUTH);		
				
		revalidate();
		repaint();

		input = new InputHandler(this);
		
		shapes[0] = new Shape(this, false);
		shapes[1]= new Shape(this, true);
	}

	/**
	 * Starts game and runs it in a loop
	 */
	public void run() {
		initialize();
		
		long time = 0;
		long lastTime = 0;
		long deltaTime = 0;
				
		while (isRunning) {
			time = System.currentTimeMillis();
			deltaTime = time - lastTime;
			lastTime = time;
			
			update(deltaTime);
			draw();

			// delay for each frame - time it took for one frame
			time = (1000 / fps) - (System.currentTimeMillis() - time);

			if (time > 0) {
				try {
					Thread.sleep(time);
				} catch (Exception e) {
				}
			}
		}

		setVisible(false);
	}

	/**
	 * Set up everything need for the game to run
	 */
	void initialize() {
		// Game variables setup
		bIndex = 1;
		isGameOver = false;
		
		List<Integer> rnd = new ArrayList<>();
		rnd.add(0); rnd.add(1); rnd.add(2);
		Collections.shuffle(rnd);
		for (int i = 0; i < boards.length; i++) {
			boards[i].initialize();
			boards[i].setColor(Colors.values()[rnd.get(i)].color);
		}
		
		nextItemPanel.setRandomShape();
		cpuItemPanel.setRandomShape();
		
		for(Shape shape : shapes) {
			shape.initialize();
			shape.setActiveBoard(boards[bIndex]);
		}
		setInitialScoreConditions();
	}
	
	void setInitialScoreConditions(){
		setScore();
		setLevel(1);
	}
	
	/**
	 * Will check for input, move things around and check conditions, etc
	 */
	void update(long delta) {
		if (getInput().hasPressed(KeyEvent.VK_I))
			initialize();
		if (getInput().hasPressed(KeyEvent.VK_P)) {
			isPaused = !isPaused;
			setLevel(level);
		}			
		
		if(isGameOver || isPaused) {
			return;
		}
		
		for(Board b : boards){
			b.update(delta);
		}
		// Change player board
		
		// Instant change to current index
		if (getInput().hasPressed(KeyEvent.VK_Q))
			setBoard(0, shapes[0]);
		if (getInput().hasPressed(KeyEvent.VK_W))
			setBoard(1, shapes[0]);
		if (getInput().hasPressed(KeyEvent.VK_E))
			setBoard(2, shapes[0]);
		
		// Increaser-decreaser of current index
		if (getInput().hasPressed(KeyEvent.VK_A))
			setBoard(bIndex - 1, shapes[0]);
		if (getInput().hasPressed(KeyEvent.VK_D))
			setBoard(bIndex + 1, shapes[0]);
	}
	
	/**
	 * This method will draw everything
	 */
	void draw() {		
		for(Board b : boards){
			b.repaint();
		}
	}
	
	/**
	 * Select given board in available boards, if collision is detected
	 * before movement, selection will not be available
	 * @param index current index to select in boards
	 */
	private void setBoard(int index, Shape shape) {
		if(bIndex == index || index < 0 || index > boards.length - 1)
			return;
		
		bIndex = index;
		
		if(!shape.willCollide(shape, boards[bIndex]))
			shape.setActiveBoard(boards[bIndex]);
	}

	/**
	 * Select board to add penalization.
	 * Will select everyone except current one
	 * @param boardColor penalizer color
	 */
	public void selectPenalized(Color boardColor) {
		for (int i = 0; i < boards.length; i++) {
			if(i != bIndex) {
				boards[i].penalize(boardColor);
			}
		}
	}
	
	/**
	 * Select boards to remove penalizations
	 * @param numberOfPardons number of penalizations to remove
	 */
	public void selectPardoned(int numberOfPardons) {
		List<Integer> ignored = new ArrayList<>();
		for (int pardon = 0; pardon < numberOfPardons; pardon++) {
			ignored.clear();			
			for (int i = 0; i < boards.length; i++)
				if(boards[i].getPenalizationCounter() <= 0) ignored.add(i);
			
			int[] ignoredInts = new int[ignored.size()];
			for (int i = 0; i < ignoredInts.length; i++)
				ignoredInts[i] = ignored.get(i);
			
			if(ignoredInts.length < boards.length)
				boards[getRandomWithExclusion(new Random(), 0, boards.length - 1, ignoredInts)].pardonize();	
		}
	}
	
	/**
	 * Get random int within range excluding some
	 * Algorithm from here: https://stackoverflow.com/a/6443346/3998458
	 * @param rnd
	 * @param start
	 * @param end
	 * @param exclude
	 * @return
	 */
	public int getRandomWithExclusion(Random rnd, int start, int end, int... exclude) {
	    int random = start + rnd.nextInt(end - start + 1 - exclude.length);
	    for (int ex : exclude) {
	        if (random < ex) {
	            break;
	        }
	        random++;
	    }
	    return random;
	}
	
	public Shape getShape(int index) {
		if(index < 0 || index > shapes.length - 1) return null;
		return shapes[index];
	}
	
	public Shape[] getShapes() {
		return shapes;
	}
		
	public InputHandler getInput() {
		return input;
	}
		
	public int getBoardIndex() {
		return bIndex;
	}
	
	public boolean isGameOver() {
		return isGameOver;
	}
	
	public void setGameOver(boolean isGameOver) {
		this.isGameOver = isGameOver;
	}
	
	public NextItemPanel getNextItemPanel() {
		return nextItemPanel;
	}
	
	public NextItemPanel getCpuItemPanel() {
		return cpuItemPanel;
	}
	
	/////////////////////////////////////////////////
	// SCORE LOGIC
	/////////////////////////////////////////////////
	
	private void setScore(){
		this.score = 0;		
		scoreLabel.setText(
				"SCORE: " + String.format("%015d", score) + "\n" +
				"NEXT LEVEL: "+ String.format("%015d", getPointsToChangeLevel()));
		scoreLabel.revalidate();
		scoreLabel.repaint();
	}
	
	public void setScore(long points) {
		this.score += points;
		if(this.score > getPointsToChangeLevel()) {
			setLevel();
		}
		
		scoreLabel.setText(
				"SCORE: " + String.format("%015d", score) + "\n" +
				"NEXT LEVEL: "+ String.format("%015d", getPointsToChangeLevel()));
		scoreLabel.revalidate();
		scoreLabel.repaint();
	}
	
	public long getScore() {
		return score;
	}

	public int getLevel() {
		return level;
	}
	
	private void setLevel() {
		this.level++;
		setLevel(this.level);
	}
	
	private void setLevel(int level) {
		this.level = level;
		
		levelLabel.setText("LEVEL: " + level + "\n" +
				"Status: " + (isGameOver 
						? "GAME OVER, hit R to restart" 
						: isPaused 
						? "PAUSED, hit P to play" 
						: "RUNNING, hit P to pause"));
		levelLabel.revalidate();
		levelLabel.repaint();
	}
	
	public int getPointsPerLineAdded() {
		return BOARD_WIDTH * 10;
	}
	
	public int getPointsPerPenalization() {
		return BOARD_WIDTH;
	}
	
	public int getPointsPerFastMovement(){
		return 1;
	}
	
	public int getPointsToChangeLevel(){
		return level * 1000;
	}
	
	private String setInstructions(){
		return "Move pieces between boards. Try form lines of pieces of same colors to remove them.\n" +
				"If a piece is placed in a board of different color, you'll be penalized and two new" +
				"random squares will be added to another boards.\n" +
				"In order to remove them, continue align horizontal lines of same color.\n" +
				"Controls: \n" +
				"LEFT RIGHT move shape inside board | UP rotate shape | DOWN for fast movement\n" +
				"SPACE automatically move shape to bottom\n" +
				"A, D move shape between boards | Q, W, E select boards 1, 2 and 3 respectively\n" + 
				"P = pause I = restart R = rotate current board, Right click = Remove active square.";
	}
	
	public Board[] getBoards() {
		return boards;
	}
}
