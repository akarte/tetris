package com.tetris.input;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.SwingUtilities;

import com.tetris.main.Tetris;
import com.tetris.point.Point;

/**
 * Handle mouse inputs
 * @author Alexandro
 *
 */
public class MouseHandler implements MouseListener {
	boolean rightClick = false;
	boolean rightOneHit = false;
	private int x;
	private int y;

	public MouseHandler(Component c) {
		x = 0;
		y = 0;
		c.addMouseListener(this);
	}
	
	private boolean isRightMouseClicked() {
		return rightClick;
	}
	
	/**
	 * Return current position in board units
	 * @return
	 */
	public Point position() {
		x = x / Tetris.SQUARE_SIZE - 1;
		x = x < 0 ? 0 : x > Tetris.BOARD_WIDTH - 1 ? Tetris.BOARD_WIDTH - 1 : x;
		y = y / Tetris.SQUARE_SIZE;
		y = y < 0 ? 0 : y > Tetris.BOARD_HEIGHT - 1 ? Tetris.BOARD_HEIGHT - 1 : y;
		return new Point(x, y);
	}
	
	/**
	 * Detect if mouse has just happened
	 * @return
	 */
	public boolean hasRightMouseClicked() {
		if(isRightMouseClicked() && !rightOneHit) {
			rightOneHit = true;
			return rightOneHit;
		}	
		return false;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (SwingUtilities.isRightMouseButton(e)) {
			x = e.getX();
			y = e.getY();
			rightClick = true;
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (SwingUtilities.isRightMouseButton(e)) {
			rightClick = false;
			rightOneHit = false;
		}
	}

}
