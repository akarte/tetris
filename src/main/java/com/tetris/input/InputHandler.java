package com.tetris.input;

import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Allows to capture keyboard inputs
 * @author Alexandro
 *
 */
public class InputHandler implements KeyListener {
	boolean[] keys = new boolean[256];
	boolean[] oneHit = new boolean[256];

	/**
	 * Assigns the newly created InputHandler to a Component
	 * 
	 * @param c Component to get input from
	 */
	public InputHandler(Component c) {
		c.addKeyListener(this);
		
		for (int i = 0; i < oneHit.length; i++) {
			oneHit[i] = false;
		}
	}

	/**
	 * Checks whether a specific key is down
	 * 
	 * @param keyCode The key to check
	 * @return Whether the key is pressed or not
	 */
	public boolean isKeyDown(int keyCode) {
		if (keyCode > 0 && keyCode < 256) {
			return keys[keyCode];
		}

		return false;
	}
	
	/**
	 * Just 1 hit
	 * @param keyCode Key to check
	 * @return True if has recently typed
	 */
	public boolean hasPressed(int keyCode) {
		if(isKeyDown(keyCode) && !oneHit[keyCode]) {
			oneHit[keyCode] = true;
			return oneHit[keyCode];
		}	
		return false;
	}
	/**
	 * Called when a key is pressed while the component is focused
	 * 
	 * @param e KeyEvent sent by the component
	 */
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() > 0 && e.getKeyCode() < 256) {
			keys[e.getKeyCode()] = true;
		}
	}

	/**
	 * Called when a key is released while the component is focused
	 * 
	 * @param e KeyEvent sent by the component
	 */
	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() > 0 && e.getKeyCode() < 256) {
			keys[e.getKeyCode()] = false;
			
			oneHit[e.getKeyCode()] = false;
		}
	}
	
	/**
	 * Not used
	 */
	public void keyTyped(KeyEvent e) {
		
	}
}