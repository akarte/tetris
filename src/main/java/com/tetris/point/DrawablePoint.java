package com.tetris.point;

import java.awt.Color;
import java.awt.Graphics;

import com.tetris.main.Tetris;

/**
 * Point that allows to be drawn in a given graphic object
 * @author Alexandro
 *
 */
public class DrawablePoint extends Point {
	public DrawablePoint(int x, int y) {
		super(x, y);
	}
	
	public void draw(Graphics g, Color color) {
		g.setColor(color);
		g.fillRect(x + 1, y + 1, Tetris.SQUARE_SIZE - 2, Tetris.SQUARE_SIZE - 2);
        g.setColor(color.brighter());
        g.drawLine(x, y + Tetris.SQUARE_SIZE - 1, x, y);
        g.drawLine(x, y, x + Tetris.SQUARE_SIZE - 1, y);
        g.setColor(color.darker());
        g.drawLine(x + 1, y + Tetris.SQUARE_SIZE - 1, x + Tetris.SQUARE_SIZE - 1, y + Tetris.SQUARE_SIZE - 1);
	    g.drawLine(x + Tetris.SQUARE_SIZE - 1, y + Tetris.SQUARE_SIZE - 1, x + Tetris.SQUARE_SIZE - 1, y + 1);
	}
}
