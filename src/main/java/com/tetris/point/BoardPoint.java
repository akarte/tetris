package com.tetris.point;

import java.awt.Color;

/**
 * Point in board
 * @author Alexandro
 *
 */
public class BoardPoint {
	public boolean isActive = false;
	private Color color;
	
	public BoardPoint(boolean isActive) {	
		this.isActive = isActive;
		this.color = Color.BLACK;
	}
	
	public Color getColor() {
		return color;
	}
	
	public void setColor(Color color) {
		this.color = color;
	}
}
