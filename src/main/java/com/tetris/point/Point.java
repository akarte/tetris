package com.tetris.point;

/**
 * Basic point to draw shapes
 * @author Alexandro
 *
 */
public class Point implements Cloneable {
	public int x;
	public int y;
	
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
}
