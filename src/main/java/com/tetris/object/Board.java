package com.tetris.object;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JPanel;

import com.tetris.input.MouseHandler;
import com.tetris.main.Tetris;
import com.tetris.point.BoardPoint;
import com.tetris.point.DrawablePoint;
import com.tetris.point.Point;

/**
 * Playable board where tetrominos are placed
 * @author Alexandro
 *
 */
public class Board extends JPanel {
	private static final long serialVersionUID = 5969842394415121855L;
	
	private final Tetris parent;
	private BufferedImage backBuffer;
	private BoardPoint[][] board;
	private Color color;
	private int penalizationCounter;
	// this let rotate graphics certain amounts of degrees
	private int flipper = 0;
	
	private MouseHandler mouse;
	
	public Board(Tetris parent) {
		this.mouse = new MouseHandler(this);
		this.parent = parent;
		this.setSize(realWidth(), realHeight());
		this.board = new BoardPoint[Tetris.BOARD_WIDTH][Tetris.BOARD_HEIGHT];
		this.penalizationCounter = 0;
		
		backBuffer = new BufferedImage(realWidth(), realHeight(), BufferedImage.TYPE_INT_ARGB);
	}
	
	public void initialize() {
		flipper = 0;
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < board[row].length; col++) {
				board[row][col] = new BoardPoint(false);
			}
		}
		penalizationCounter = 0;
	}
	
	public void update(long delta) {	
		if(parent.isGameOver()) return;
		
		for (int i = 0; i < parent.getShapes().length; i++) {
			Shape shape = parent.getShapes()[i];
			if(shape.getActiveBoard() == this) {
				shape.update(delta);
			}
		}
				
		if (mouse.hasRightMouseClicked()) {
			removeIfActive(mouse.position());
		}
		
		
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		
		AffineTransform transform = new AffineTransform();
		transform.rotate(Math.toRadians(flipper), getWidth() / 2, (getHeight() - Tetris.BOARD_HEIGHT) / 2 - 1);
		//AffineTransform old = g2.getTransform();
		g2.transform(transform);

		// draw your rectangle here...
		draw(g2);
	}
	
	private void draw(Graphics g) {
		Graphics bbg = backBuffer.getGraphics();

		bbg.setColor(getDarkerColor(color));	
		bbg.fillRect(0, 0, realWidth(), realHeight());
		
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < board[row].length; col++) {
				BoardPoint bp = board[row][col];
				// Draw just those active ones
				if(bp.isActive) {
					DrawablePoint p = new DrawablePoint(row * Tetris.SQUARE_SIZE, col * Tetris.SQUARE_SIZE);
					p.draw(bbg, bp.getColor());
				}
			}
		}
		
		// Draw shapes
		for(Shape shape : parent.getShapes()) {
			if(shape.getActiveBoard() == this)
				shape.draw(bbg);
		}
		
		int x = (this.getWidth() - backBuffer.getWidth(null)) / 2;
	    //int y = (this.getHeight() - backBuffer.getHeight(null)) / 2;
		g.drawImage(backBuffer, x, 0, this);
	}
	
	private void removeIfActive(Point p) {
		System.out.println("Point at" + p.x + "," + p.y);
		if(flipper == 180) {
			int auxX = p.x; 
			int auxY = p.y;
			p.x = Math.abs(auxX - Tetris.BOARD_WIDTH) - 1;
			p.y = Math.abs(auxY - Tetris.BOARD_HEIGHT) - 1;
			System.out.println("Point at" + p.x + "," + p.y);
		}
		if(!board[p.x][p.y].isActive) return;
				
		for (int col = p.y; col >= 0; col--) {
			if(col > 0)	board[p.x][col] = board[p.x][col - 1];
			else 		board[p.x][col].isActive = false;
		}
		
		// CHECK THIS!!
		//penalizationCounter--;
		// parent.setScore(parent.getPointsPerPenalization() * penalizationCounter);
		
		int filledLines = checkFilledLines();
		if(filledLines > 0){
			parent.selectPardoned(filledLines);
		}
	}

	/**
	 * Activate a random point at the bottom of the board
	 * with a penalization color (a color different to boardColor)
	 * @param color Penalization color in random point
	 */
	public void penalize(Color color) {
		int x = (int) (Math.random() * Tetris.BOARD_WIDTH);
		int y = shortestBottom(x, 0) - 1;
		
		board[x][y].isActive = true;
		board[x][y].setColor(color);
		
		penalizationCounter++;
		parent.setScore(parent.getPointsPerPenalization() * penalizationCounter * -1);
	}
	
	public void pardonize() {
		Point p = getIndexToPardonize();
		
		if(null == p) return;
				
		for (int col = p.y; col >= 0; col--) {
			if(col > 0)	board[p.x][col] = board[p.x][col - 1];
			else 		board[p.x][col].isActive = false;
		}
		
		penalizationCounter--;
		parent.setScore(parent.getPointsPerPenalization() * penalizationCounter);
		
		int filledLines = checkFilledLines();
		if(filledLines > 0){
			parent.selectPardoned(filledLines);
		}
	}
	
	private Point getIndexToPardonize(){
		for (int col = 0; col < Tetris.BOARD_HEIGHT; col++) {
			for (int row = 0; row < Tetris.BOARD_WIDTH; row++) {
				BoardPoint bp = board[row][col];
				if(bp.isActive && !bp.getColor().equals(color)){
					return new Point(row, col);
				}
			}
		}
		return null;
	}
		
	/**
	 * Apply shape and color to board in given offset/position
	 * @param points
	 * @param offset
	 * @param color
	 */
	public void shapeToBoard(LinkedList<Point> points, Point offset, Color color) {
		for(Point sp : points) {
			board[sp.x + offset.x][sp.y + offset.y].isActive = true;
			board[sp.x + offset.x][sp.y + offset.y].setColor(color);
		}
		
		int filledLines = checkFilledLines();
		if(filledLines > 0)
			parent.selectPardoned(filledLines);
	}
	
	/**
	 * Check filled rows, and remove them
	 * @return number of filled lines found
	 */
	private int checkFilledLines() {
		List<Integer> linesToRemove = new ArrayList<>();
		
		for (int col = 0; col < Tetris.BOARD_HEIGHT; col++) {
			boolean flag = true;
			for (int row = 0; row < Tetris.BOARD_WIDTH; row++) {
				BoardPoint bp = board[row][col];
				if(!bp.isActive || bp.isActive && !bp.getColor().equals(color)){
					flag = false;
					break;
				}
			}
			if(flag) linesToRemove.add(col);
		}
		
		// If filled rows are found, remove them and check for penalizations to remove
		for (Integer lineToRemove : linesToRemove) {
			removeLine(lineToRemove);
			parent.setScore(parent.getPointsPerLineAdded());
		}
		
		return linesToRemove.size();
	}
	
	/**
	 * Removes given row/line in board.
	 * For this, copy everything from given row position minus one to
	 * under that board. 
	 * @param line Row to be removed from board
	 */
	private void removeLine(int line){
		for (int col = line; col >= 0; col--) {
			for (int row = Tetris.BOARD_WIDTH - 1; row >= 0; row--) {
				if(col > 0) {
					board[row][col] = board[row][col - 1];
				} else {
					board[row][col].isActive = false;
				}
			}
		}
	}

	/**
	 * Return the distance from coordinate given to the most Y active
	 * point. 
	 * If no active points are found, returns the height of the board.
	 * @param x the X coordinate
	 * @param y the Y coordinate
	 * @return Distance from point (x,y) from nearest Y active distance 
	 */
	public int shortestBottom(int x, int y) {
		for (int col = y; col < board[x].length; col++) {
			if (board[x][col].isActive)
				return col;
		}
		return board[x].length;
	}
	
	public BoardPoint[][] getBoard() {
		return board;
	}
	
	public Color getBoardColor() {
		return color;
	}
	
	public void setColor(Color color) {
		this.color = color;
	}
	
	public int getPenalizationCounter() {
		return penalizationCounter;
	}
	
	public void addPenalization(int penalization) {
		this.penalizationCounter += penalization;
	}

	private int realWidth() {
		return Tetris.BOARD_WIDTH * Tetris.SQUARE_SIZE;
	}
	
	private int realHeight() {
		return Tetris.BOARD_HEIGHT * Tetris.SQUARE_SIZE;
	}
	
	private Color getDarkerColor(Color color) {
		int factor = 8;
		return new Color(color.getRed() / factor, color.getGreen() / factor, color.getBlue() / factor);
	}
	
	public void flip() {
		flipper = flipper == 180 ? 0 : 180;
	}
}
