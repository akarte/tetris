package com.tetris.object;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import com.tetris.main.Tetris;
import com.tetris.object.Tetromino.Type;
import com.tetris.point.DrawablePoint;
import com.tetris.point.Point;

/**
 * Principal class that abstract a tetris shape
 * @author Alexandro
 *
 */
public class Shape {
	private Tetromino tetromino;
	private Tetris parent;
	private Point position;
	private Color color;
	private Board activeBoard;
	private long elapsedTime;
	private boolean rotOrientation = true;
	private boolean autoplay = false;
	private long MOVE_TIME = 0;
	
	public Shape(Tetris parent, boolean autoplay) {
		this.parent = parent;
		this.position = new Point(0, 0);
		this.autoplay = autoplay;
	}
	
	/**
	 * Reset shape position and elapsedTime.
	 * Set a new random tetromino and random color.
	 */
	public void initialize(){
		if(!autoplay)
			this.position.x = Tetris.BOARD_WIDTH / 4 * 2 - 1;
		else
			this.position.x = Tetris.BOARD_WIDTH / 4 * 3 + 1;
		
		this.position.y = 1;
		
		if(!autoplay) {
			this.color = parent.getNextItemPanel().getColor();
			this.tetromino = parent.getNextItemPanel().getTetromino();
			parent.getNextItemPanel().setRandomShape();
			parent.getNextItemPanel().repaint();
		} else {
			MOVE_TIME = 0;
			this.color = parent.getCpuItemPanel().getColor();
			this.tetromino = parent.getCpuItemPanel().getTetromino();
			parent.getCpuItemPanel().setRandomShape();
			parent.getCpuItemPanel().repaint();
		}
		
		elapsedTime = 0;
		
		if(null != activeBoard && willCollide(getPoints(), position.x, position.y)) {
			parent.setGameOver(true);
			tetromino = Tetromino.fabric(Type.NullShape);
		}
	}
	
	/**
	 * Update shape
	 * (Where logic is triggered)
	 * @param delta
	 */
	public void update(long delta) {
		if(parent.isGameOver()) return;
						
		// Speed of falling is given by DROP_LOOP_TIME
		long dropTime = Tetris.DROP_LOOP_TIME - (parent.getLevel() * Tetris.DROP_TIME_DECREASER);
		dropTime = dropTime < 0 ? 0 : dropTime;
		
		if (elapsedTime > dropTime) {
			// If at bottom, apply to board and reset
			if(verticalMove(1)){
				shapeToBoard();
			} else {
				elapsedTime = 0;
			}
		}
		
		if(autoplay) {
			if(MOVE_TIME > 200) {
				heuristic();	
				MOVE_TIME = 0;
			}
			MOVE_TIME += delta;
			elapsedTime += delta;
			return; 
		}

		if(!autoplay) {
			if(parent.getInput().hasPressed(KeyEvent.VK_R)) {
				activeBoard.flip();
			}
		}
		
		if (parent.getInput().hasPressed(KeyEvent.VK_RIGHT))
			horizontalMove(1);
		if (parent.getInput().hasPressed(KeyEvent.VK_LEFT)) 
			horizontalMove(-1);
		if (parent.getInput().hasPressed(KeyEvent.VK_UP))
			rotate();
		if (parent.getInput().isKeyDown(KeyEvent.VK_DOWN)){
			if(verticalMove(1)){
				shapeToBoard();
			}
			// Reward player for fast movements
			parent.setScore(parent.getPointsPerFastMovement());
		}
		if (parent.getInput().hasPressed(KeyEvent.VK_SPACE)){
			int nearestBottom = getNearestBottom(position, tetromino.getPoints());
			if(verticalMove(nearestBottom)){
				shapeToBoard();	
			}
			// Reward player for fast movements
			parent.setScore(nearestBottom);
		}
			
		elapsedTime += delta;
	}
		
	/**
	 * Draw shape
	 * @param g Graphics object where shape will be drawn
	 */
	public void draw(Graphics g) {
		for (Point point : tetromino.getPoints()) {
			DrawablePoint p = new DrawablePoint(point.x * Tetris.SQUARE_SIZE, point.y * Tetris.SQUARE_SIZE);
			p.x += position.x * Tetris.SQUARE_SIZE;
			p.y += position.y * Tetris.SQUARE_SIZE;
			
			p.draw(g, color);
		}
	}
	
	/**
	 * Rotate shape.
	 * OShape (or square shape) should not be rotated.
	 * ZShape and SShape and IShape should only be rotated 
	 * clockwise and counter-clockwise intermittently.
	 * LShape, JShape and TShape should be rotated only clockwise.
	 */
	private void rotate() {
		// If shape is Square, then do nothing
		if(tetromino.getType() == Type.OShape) return;
		
		LinkedList<Point> temp = new LinkedList<>();
		for (Point p : tetromino.getPoints()) {
			// Clone point
			Point point = new Point(p.x, p.y);
			if(tetromino.getType() == Type.SShape 
					|| tetromino.getType() == Type.ZShape 
					||	tetromino.getType() == Type.IShape) {
				if(rotOrientation) {
					// Rotate clockwise
					point.x = -p.y;
					point.y =  p.x;
				} else {
					// Rotate counterclockwise
					point.x =  p.y;
					point.y = -p.x;
				}
			}
			if(tetromino.getType() == Type.LShape 
					|| tetromino.getType() == Type.JShape 
					||	tetromino.getType() == Type.TShape) {
				// Rotate clockwise
				point.x = -p.y;
				point.y =  p.x;	
			}
			
			temp.add(point);
		}
		
		// If rotation is valid, apply it
		if(!willCollide(temp, position.x, position.y) && 
				!willCollide(this, temp, position, parent.getShapes())){
			tetromino.setPoints(temp);
			rotOrientation = !rotOrientation;
		}
	}
	
	/**
	 * Once shape hit most posible bottom area, this method should be called
	 * to apply this shape to current active board.
	 */
	private void shapeToBoard() {
		// If color in shape is different in activeBoard color
		// then apply penalization:
		// Two random points will be removed and added to the other two boards
		if(activeBoard.getBoardColor() != null && color != activeBoard.getBoardColor()) {
			int ran = 0;
			for(int i = 0; i < Tetris.PENALIZATION; i++) {
				ran = (int) Math.random() * getPoints().size();
				getPoints().remove(ran);
			}
			activeBoard.addPenalization(2);
			parent.selectPenalized(activeBoard.getBoardColor());
		}
		// Apply shape to board in given position and with specific color
		activeBoard.shapeToBoard(getPoints(), position, color);
		// After apply it, reset shape to top area and new random shape
		initialize();
	}
			
	/**
	 * Lateral movements, if collision is found, movement is cancelled.
	 * @param step number of steps to move (negative means left)
	 */
	private void horizontalMove(int step) {
			position.x = !willCollide(getPoints(), position.x + step, position.y)
					&& !willCollide(this, new Point(position.x + step, position.y), parent.getShapes())
					? position.x + step : position.x;
	}
	
	/**
	 * Move shape vertically.
	 * If collision is found before, movement will be cancelled.
	 * @param step Number of steps to be moved
	 * @return true if collision is found when trying to move (cancelling it)
	 */
	private boolean verticalMove(int step) {
		boolean isHitting =  willCollide(getPoints(), position.x, position.y + step, activeBoard);
		//boolean isHitAnotherShape = willCollide(this, new Point(position.x, position.y + step), parent.getShapes());
		
		if(isHitting) return true;
		
		// CHECK THIS
		//if(isHitAnotherShape)
			//return false;
		//else
			position.y = position.y + step;
		return false;
	}
	
	/**
	 * Given an offset, and a set of points, this method will find for the shortest
	 * distance from those points to the nearest available bottom in current active board.
	 * @param offset Displacement in x and y
	 * @param points Set of points that need to be the distance to be calculated
	 * @return The shortest distance found
	 */
	private int getNearestBottom(Point offset, LinkedList<Point> points) {
		return getNearestBottom(offset, points, activeBoard);
	}
	
	/**
	 * Given an offset, and a set of points, this method will find for the shortest
	 * distance from those points to the nearest available bottom in a board.
	 * @param offset Displacement in x and y
	 * @param points Set of points that need to be the distance to be calculated
	 * @param board Set of points where distance will be calculated
	 * @return The shortest distance found
	 */
	private int getNearestBottom(Point offset, LinkedList<Point> points, Board board) {
		int bottom = Tetris.BOARD_HEIGHT;
		for(Point p : points) {
			// For current shape/point, get nearest active shape/point under it
			int nearestY = board.shortestBottom(p.x + offset.x, p.y + offset.y + 1);
			// get distance from nearestY to that point
			bottom = Math.min(bottom, nearestY - (p.y + offset.y + 1));
		}
		return bottom;
	}
	
	/**
	 * Check for collision with given shape object and current active board.
	 * Collision occur when top/bottom is detected or when two shapes collide.
	 * @param shape Shape object that should be checked for collision
	 * @param board Set of points to check collision
	 * @return true if collision will happen
	 */
	public boolean willCollide(Shape shape, Board board){
		return willCollide(shape.getPoints(), shape.getPosition().x, shape.getPosition().y, board);
	}

	/**
	 * Check for collision with given set of points with an offset (in x and/or y) and 
	 * current active board.
	 * Collision occur when top/bottom is detected or when two shapes collide.
	 * @param points Set of points that should be checked for collision
	 * @param xOffset X displacement
	 * @param yOffset X displacement
	 * @return true if collision will happen
	 */
	private boolean willCollide(List<Point> points, int xOffset, int yOffset) {
		return willCollide(points, xOffset, yOffset, activeBoard);
	}

	/**
	 * Check for collision with given set of points with an offset (in x and/or y) and 
	 * specific board.
	 * Collision occur when top/bottom is detected or when two shapes collide.
	 * @param points Set of points that should be checked for collision
	 * @param xOffset X displacement
	 * @param yOffset Y displacement
	 * @param board Set of points to check collision
	 * @return true if collision will happen
	 */
	private boolean willCollide(List<Point> points, int xOffset, int yOffset, Board board){
		for (int i = 0; i < points.size(); i++) {
			// Shape hitting top or bottom
			if(points.get(i).y + yOffset < 0 || points.get(i).y + yOffset > Tetris.BOARD_HEIGHT - 1)
				return true;
			// Shape hitting walls
			if(points.get(i).x + xOffset < 0 || points.get(i).x + xOffset > Tetris.BOARD_WIDTH - 1)
				return true;
			// Shape hitting squares in board		
			if(board.getBoard()[points.get(i).x + xOffset][points.get(i).y + yOffset].isActive)
				return true;
		}		
		return false;
	}
	
	/**
	 * Collission between shapes
	 * @param points1
	 * @param p1Offset
	 * @param points2
	 * @param p2Offset
	 * @return
	 */
	private boolean willCollide(Shape sA, Point p1Offset, Shape[] shapes) {	
		List<Point> points1 = sA.getPoints();
		for (Shape shape : shapes) {
			if(sA == shape) 
				continue;
			if(sA.getActiveBoard() != shape.getActiveBoard())
				continue;
			
			
			List<Point> points2 = shape.getPoints();
			Point p2Offset = shape.position;
			
			for (int i = 0; i < points1.size(); i++) {
				for (int j = 0; j < points2.size(); j++) {
					Point a = points1.get(i);
					Point b = points2.get(j);
					if(a.x + p1Offset.x == b.x + p2Offset.x && a.y + p1Offset.y == b.y + p2Offset.y) {
						return true;
					}
				}
			}	
		}
		return false;
	}
	
	private boolean willCollide(Shape sA, List<Point> points1, Point p1Offset, Shape[] shapes) {
		for (Shape shape : shapes) {
			System.out.println(sA + " " + shape);
			if(sA == shape) 
				continue;
			if(sA.getActiveBoard() != shape.getActiveBoard())
				continue;
			
			System.out.println("check");
			
			List<Point> points2 = shape.getPoints();
			Point p2Offset = shape.position;
			
			for (int i = 0; i < points1.size(); i++) {
				for (int j = 0; j < points2.size(); j++) {
					Point a = points1.get(i);
					Point b = points2.get(j);
					if(a.x + p1Offset.x == b.x + p2Offset.x && a.y + p1Offset.y == b.y + p2Offset.y) {
						System.out.println("collision");
						return true;
					}
				}
			}	
		}
		return false;
	}
			
	public LinkedList<Point> getPoints() {
		return tetromino.getPoints();
	}
	
	public Point getPosition() {
		return position;
	}
	
	public Board getActiveBoard() {
		return activeBoard;
	}
	
	public void setActiveBoard(Board board) {
		activeBoard = board;
	}
	

	private boolean checkColor() {
		if(color != activeBoard.getBoardColor()) {
			setActiveBoard(
					parent.getBoards()[
		                   parent.getRandomWithExclusion(
		                		   new Random(), 
		                		   0, 
		                		   parent.getBoards().length - 1,
		                		   parent.getBoardIndex())
					]);
			return false;
		}
		
		return true;
	}
	
	private void heuristic() {
		if(checkColor()) {
			horizontalMove(getBestHorizontal());
		}
	}
	
	private int getBestHorizontal2() {
		return Math.random() > 0.5 ? 1 : -1;
	}
	
	private int getBestHorizontal() {
		if(tetromino.getType() == Type.OShape) {
			int maxNB = 0;
			int h = 0;
			for (int i = 0; i < Tetris.BOARD_WIDTH - 1; i++) {
				int nB = getNearestBottom(new Point(i, position.y), getPoints());
				System.out.println(maxNB + " " + nB);
				if(maxNB > nB) {
					h = i;
				}
			}
			
			position.x = h;
		}
		
		return 1;
	}
}

