package com.tetris.object;

import java.util.LinkedList;

import com.tetris.point.Point;

public class Tetromino {
	private static final Point[][] pointsMatrix = new Point[][]{ 
			new Point[]{new Point( 0, 0),new Point( 0, 0),new Point( 0, 0),new Point( 0, 0)},    // NULL
			new Point[]{new Point(-1,-1), new Point( 0,-1), new Point( 0, 0), new Point( 1, 0)}, // Z
			new Point[]{new Point( 1,-1), new Point( 0,-1), new Point( 0, 0), new Point(-1, 0)}, // S
			new Point[]{new Point( 0,-1), new Point( 0, 0), new Point( 0, 1), new Point( 0, 2)}, // I
			new Point[]{new Point(-1, 0), new Point( 0, 0), new Point( 1, 0), new Point( 0, 1)}, // T
			new Point[]{new Point( 0, 0), new Point( 1, 0), new Point( 0,-1), new Point( 1,-1)}, // O
			new Point[]{new Point( 0,-1), new Point( 0, 0), new Point( 0, 1), new Point( 1, 1)}, // L
			new Point[]{new Point( 0,-1), new Point( 0, 0), new Point( 0, 1), new Point(-1, 1)}  // J
	};
	
	enum Type {
		NullShape, ZShape, SShape, IShape, TShape, OShape, LShape, JShape
	}
	
	private LinkedList<Point> points;
	private final Type type;
	
	public static Tetromino fabric(Type type) {
		return new Tetromino(pointsMatrix[type.ordinal()], type);
	}
	
	private Tetromino(Point[] points, Type type) {
		this.type = type;
		this.points = new LinkedList<>();
		for (int i = 0; i < points.length; i++) {
			this.points.add(new Point(points[i].x, points[i].y));
		}
	}
	
	public Type getType() {
		return type;
	}
		
	public LinkedList<Point> getPoints() {
		return points;
	}
	
	public void setPoints(LinkedList<Point> points) {
		this.points = points;
	}
}
