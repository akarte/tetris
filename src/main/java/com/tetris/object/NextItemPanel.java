package com.tetris.object;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import com.tetris.main.Tetris;
import com.tetris.object.Tetromino.Type;
import com.tetris.point.DrawablePoint;
import com.tetris.point.Point;

/**
 * Help panel to show next item to drop
 * @author Alexandro
 *
 */
public class NextItemPanel extends JPanel {
	private static final long serialVersionUID = -8566701836665088261L;
	
	private BufferedImage backBuffer;
	private int width = 4 * Tetris.SQUARE_SIZE;
	private int height = 4 * Tetris.SQUARE_SIZE;
	
	private Color color;
	private Tetromino tetromino;
	
	public NextItemPanel() {
		this.setSize(width, height);
		this.setPreferredSize(new Dimension(width, height));
		this.setBackground(Color.BLACK);
		backBuffer = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
	}
	
	public void setRandomShape() {
		int ran = (int) (Math.random() * 3);
		this.color = Tetris.Colors.values()[ran].getColor();
		ran = (int) (Math.random() * 7) + 1;
		this.tetromino = Tetromino.fabric(Type.values()[ran]);
	}

	public Tetromino getTetromino() {
		return tetromino;
	}
	
	public Color getColor() {
		return color;
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		draw(g);
	}
	
	public void draw(Graphics g) {
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, width, height);
		
		Graphics bbg = backBuffer.getGraphics();

		bbg.setColor(Color.BLACK);	
		bbg.fillRect(0, 0, width, height);
		
		int minX = 0, maxX = 0, minY = 0, maxY = 0;
		for(Point point: tetromino.getPoints()) {
			minX = Math.min(minX, point.x); maxX = Math.max(maxX, point.x);
			minY = Math.min(minY, point.y); maxY = Math.max(maxY, point.y);
		}

		int x = (Tetris.SQUARE_SIZE * (maxX - minX)) / 2;
	    int y = (Tetris.SQUARE_SIZE * (maxY - minY)) / 2;
	    
		for(Point point: tetromino.getPoints()) {			
			DrawablePoint p = new DrawablePoint( 
					(point.x - minX) * Tetris.SQUARE_SIZE,	
					(point.y - minY) * Tetris.SQUARE_SIZE);
			p.draw(bbg, color);
		}
				
		x = (this.getWidth() - (Tetris.SQUARE_SIZE * (maxX - minX))) / 2;
	    y = (this.getHeight() - (Tetris.SQUARE_SIZE * (maxY - minY))) / 2;
		g.drawImage(backBuffer, x - 10, y - 10, this);
	}
}
